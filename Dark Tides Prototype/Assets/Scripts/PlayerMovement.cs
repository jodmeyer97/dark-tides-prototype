﻿using UnityEngine;
using UnityEngine.AI;

public class PlayerMovement : MonoBehaviour
{
    NavMeshAgent agent;
    AudioSource motoraudio;
    Vector3 destination;
    public Transform target;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        motoraudio = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;

            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 10000))
            {
                motoraudio.Play();
                agent.destination = hit.point;
                
            }
        }
    }
}


